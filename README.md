This is a modified version of 1NutWunder's [KZTimer Jumpstat plugin](https://forums.alliedmods.net/showthread.php?t=252392) which adds ownages and perfect jump type to every jump type.

If you find any issues, please post them [in here](https://bitbucket.org/GameChaos/kzjumpstats/issues?status=new&status=open) with an accurate description of how to reproduce it/how it happens.